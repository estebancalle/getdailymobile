// Import the functions you need from the SDKs you need
import * as firebase from "firebase/compat/app";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyBrlb_1bluC1vbIuvxYD7QkTTC2pZvNCKE",
  authDomain: "usuarios-8f665.firebaseapp.com",
  databaseURL: "https://usuarios-8f665.firebaseio.com",
  projectId: "usuarios-8f665",
  storageBucket: "usuarios-8f665.appspot.com",
  messagingSenderId: "226246172555",
  appId: "1:226246172555:web:13476d3d5248bf4e61d98e",
  measurementId: "G-RE3GS3C0SG"
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);
export default firebase;