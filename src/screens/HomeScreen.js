import React from 'react';
import { Stack, Button, Center, NativeBaseProvider,AddIcon, Box, Checkbox, Ionicons, HStack, VStack, IconButton, Container,Heading } from "native-base";

import axios from "axios";

let datos = []

class HomeScreen extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            tasks:[]
        };
    }


    async authState(){
        try {
            const resp = await axios.get('https://backendgetdailymobile.vercel.app/authstate');
            if(resp.data.resultado==="false"){
                this.props.navigation.navigate('Login')
            }
        } catch (err) {
            // Handle Error Here
            alert(err)
        }
    }

    async getTaskToday(){
        try {
            const resp = await axios.get('https://backendgetdailymobile.vercel.app/getasks/'+this.props.route.params.email);
            this.setState({tasks: resp.data.resultado})
        } catch (err) {
            // Handle Error Here
            alert(err)
        }
    }

    componentDidMount(){
        this.authState()
        this.getTaskToday()
    }

    render() {

        const handlerLogout = async () => {
            try {
                const resp = await axios.get('https://backendgetdailymobile.vercel.app/logout');
                if(resp.data.resultado==="true"){
                    this.props.navigation.navigate('Login')
                }
            } catch (err) {
                // Handle Error Here
                alert(err)
            }
        }

        const handleChange = (event) => {
            if(event){
                alert("Checked");
            }
            else{
                alert("Unchecked");
            }
          };

        const update = ()=>{
            this.getTaskToday()
        }

        const updateTag = async ()=>{

        }

    
        return (

            <NativeBaseProvider>
                <Center flex={1}>
                    <Heading>Mis Tareas</Heading>
                    <Box>
                        <Checkbox.Group accessibilityLabel="choose values">
                            {this.state.tasks.map(el => <Checkbox value={el.id} my={2} onChange={handleChange()}>{el.taskName}</Checkbox>)} 
                        </Checkbox.Group>
                    </Box>{'\n'}
                    <Stack direction="row" mb="2.5" mt="1.5" space={2}>
                        <Button size="sm" colorScheme="primary" onPress={update}>Actualizar</Button>
                        <Button size="sm" colorScheme="red" onPress={handlerLogout}>Cerrar Sesión</Button>
                    </Stack>
                </Center>
                
            </NativeBaseProvider>
        );
    }
}

// ...

export default HomeScreen;