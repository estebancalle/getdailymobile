import React, {useState} from "react";
import { Text, Button, Center, NativeBaseProvider,Image, Box, AspectRatio, Stack, Heading, HStack, Input,FormControl, Link  } from "native-base";

import axios from "axios";

class LoginScreen extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password:''
        };
    }

  render() {

    const handleCreateAccount = async () => {

        let datosRequest  = {
            email: this.state.email,
            password: this.state.password
        }

        try {
            const resp = await axios.post('https://backendgetdailymobile.vercel.app/signUp', datosRequest);
            alert(resp.data.message)
            this.props.navigation.navigate('Login')
        } catch (err) {
            // Handle Error Here
            alert(err)
        }
    }

    return (
        <NativeBaseProvider>
            <Center flex={1}>
                <Box alignItems="center">
                    <Box maxW="80" rounded="lg" overflow="hidden" borderColor="coolGray.200" borderWidth="1" _dark={{borderColor: "coolGray.600",backgroundColor: "gray.700"}} _web={{shadow: 2,borderWidth: 0}} _light={{backgroundColor: "gray.50"}}>
                    <Box>
                        <AspectRatio w="100%">
                            <Image source={{uri: "https://i.ibb.co/7W7Kwzg/Mobile-login-cuate.png"}} alt="image" />
                        </AspectRatio>
                        <Center bg="blue.500" _dark={{bg: "violet.400"}} _text={{color: "warmGray.50",fontWeight: "700",fontSize: "xs"}} position="absolute" bottom="0" px="3" py="1.5">
                            Registro
                        </Center>
                    </Box>
                    <Stack p="4" space={3}>
                        <Stack space={2}>
                            <Heading size="md" ml="-1">
                                Asocia tu cuenta de GetDaily
                            </Heading>
                            <Text fontSize="xs" _light={{color: "blue.500"}} _dark={{color: "blue.400"}} fontWeight="500" ml="-0.5" mt="-1">
                                Crea una cuenta con tu correo asociado a la versión web
                            </Text>
                    </Stack>
                    <Box alignItems="center">
                        <FormControl.Label>Correo</FormControl.Label>
                        <Input type="email" defaultValue="" placeholder="email" onChangeText={(text) =>this.setState({email:text})} />
                        <FormControl.Label>Conteaseña</FormControl.Label>
                        <Input type="password" defaultValue="" placeholder="password" onChangeText={(text) =>this.setState({password:text})} />{'\n'}
                        <Button onPress={handleCreateAccount} colorScheme="blue">Crear Cuenta</Button>{'\n'}
                        <Link onPress={() =>this.props.navigation.navigate('Login')}>¿Ya tienes una cuenta?, inicia sesión</Link>

                    </Box>
                    <HStack alignItems="center" space={4} justifyContent="space-between">
                        <HStack alignItems="center">
                            <Text color="coolGray.600" _dark={{color: "warmGray.200"}} fontWeight="200">
                                V 1.0
                            </Text>
                        </HStack>
                    </HStack>
                    </Stack>
                </Box>
                </Box>
            </Center>
        </NativeBaseProvider>
    );
  }
}

// ...

export default LoginScreen;