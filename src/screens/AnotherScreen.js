import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

class AnotherScreen extends React.Component {
  render() {
    return (
      <View>
        <Text>Another Screen</Text>
      </View>
    );
  }
}

// ...

export default AnotherScreen;