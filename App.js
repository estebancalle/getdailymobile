import React from "react";
import { Text, Button, Center, NativeBaseProvider,Image,extendTheme, Box, AspectRatio, Heading, HStack } from "native-base";

// Importamos las pantallas
import LoginScreen from "./src/screens/LoginScreen";
import HomeScreen from "./src/screens/HomeScreen";
import AnotherScreen from "./src/screens/AnotherScreen";
import RegisterScreen from "./src/screens/RegisterScreen";


import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack'

const Stack = createNativeStackNavigator();
// const theme = extendTheme({
//   fontConfig: {
//     Roboto: {
//       100: {
//         normal: "Roboto-Light",
//         italic: "Roboto-LightItalic",
//       },
//       200: {
//         normal: "Roboto-Light",
//         italic: "Roboto-LightItalic",
//       },
//       300: {
//         normal: "Roboto-Light",
//         italic: "Roboto-LightItalic",
//       },
//       400: {
//         normal: "Roboto-Regular",
//         italic: "Roboto-Italic",
//       },
//       500: {
//         normal: "Roboto-Medium",
//       },
//       600: {
//         normal: "Roboto-Medium",
//         italic: "Roboto-MediumItalic",
//       },
//       // Add more variants
//       //   700: {
//       //     normal: 'Roboto-Bold',
//       //   },
//       //   800: {
//       //     normal: 'Roboto-Bold',
//       //     italic: 'Roboto-BoldItalic',
//       //   },
//       //   900: {
//       //     normal: 'Roboto-Bold',
//       //     italic: 'Roboto-BoldItalic',
//       //   },
//     },
//     fonts: {
//       heading: "Roboto",
//       body: "Roboto",
//       mono: "Roboto",
//     }
// }});

  export default () => {
      return (
      <NavigationContainer>
        <Stack.Navigator screenOptions={{headerBackVisible: false}}>
          <Stack.Screen name="Login" component={LoginScreen} options={{ headerShown: false }} />
          <Stack.Screen name="Home" component={HomeScreen} options={{ headerShown: true } }  />
          <Stack.Screen name="Another" component={AnotherScreen} />
          <Stack.Screen name="Register" component={RegisterScreen} />
        </Stack.Navigator>
      </NavigationContainer>
      );
  };
  